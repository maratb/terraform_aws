# Terraform lessons
#
#
provider "aws" {
    #region     = "ap-northeast-1" env AWS_DEFAULT_REGION
}

resource "aws_instance" "test-ubuntu" {
    count         = 2  
    ami           = "ami-0590f3a1742b17914"
    instance_type = "t2.micro"

    tags = {
        Name = "Ubuntu"
        Owner = "Marat Biryushev"
        Project = "Terraform Lessons"
  }
}

resource "aws_instance" "test-amazon" {
    ami           = "ami-072bfb8ae2c884cc4"
    instance_type = "t2.micro"

    tags = {
        Name  = "Amazon"
        Owner = "Marat Biryushev"
        Project = "Terraform Lessons"        
  }
}